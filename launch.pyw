from math import ceil
from cvrp.get_data.clients import get_random_points_in_warsaw
from cvrp.ui.main import launch_ui
DEBUG = False

if __name__ == "__main__":

    network = None

    if not DEBUG:
        from cvrp.data import Network, Place, Vehicle
        from numpy import random
        import string

        network = Network()

        random.seed(42)

        network.depot.latitude = 52.23215105870395
        network.depot.longitude = 21.006132620393785
        network.depot.start_time = 0.0
        network.depot.end_time = 100

        num_clients = 7
        avg_cl_per_vh = 4
        d_min, d_max = 6, 12

        # Add clients
        # clients_in_warsaw = get_random_points_in_warsaw(num_clients)

        MAX_TRAVEL_TIME = 100
        clients_in_warsaw = [
            {'id': 1, 'lon': 20.96478365296623, 'lat': 52.20992050618454, 'start_time': 0.0, 'end_time': MAX_TRAVEL_TIME},
            {'id': 2, 'lon': 21.021605083015544, 'lat': 52.21326072392674, 'start_time': 0.0, 'end_time': MAX_TRAVEL_TIME},
            # this time window indicates that their package must be delivered early:
            {'id': 3, 'lon': 20.994964897501, 'lat': 52.21532661616765, 'start_time': 0.0, 'end_time': 1},
            # this time window indicates that their package must be delivered late:
            {'id': 4, 'lon': 20.957890156436044, 'lat': 52.21278605454256, 'start_time': 100.0, 'end_time': 120},
            {'id': 5, 'lon': 20.96152427076991, 'lat': 52.23250031269491, 'start_time': 0.0, 'end_time': MAX_TRAVEL_TIME},
            # this time window indicates that their package must be delivered early:
            {'id': 6, 'lon': 21.041960879169455, 'lat': 52.220472911872996, 'start_time': 0.0, 'end_time': 1},
            {'id': 7, 'lon': 21.04752566284914, 'lat': 52.247594209447236, 'start_time': 0.0, 'end_time': MAX_TRAVEL_TIME}
        ]

        for i, client in enumerate(clients_in_warsaw):
            print(i, client["start_time"], client["end_time"])
            network.add_client(Place(
                name=f"Klient {i + 1}",
                lat=client["lat"],
                lon=client["lon"],
                demand=1,
                # deepcopy:
                start_time=client["start_time"],
                end_time=client["end_time"]
            ))

        num_vehicles = ceil(num_clients / avg_cl_per_vh)
        c_min, c_max = d_min * avg_cl_per_vh, d_max * avg_cl_per_vh

        # Add vehicles
        for k in range(num_vehicles):
            network.add_vehicle(Vehicle(
                name=f"Pojazd {string.ascii_uppercase[k]}",
                max_capacity=random.randint(c_min * 10, c_max * 10) / 1
            ))

    launch_ui(network)
