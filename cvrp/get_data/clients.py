import random
from typing import List, Dict
import numpy as np

min_lon = 20.95
max_lon = 21.05
min_lat = 52.206
max_lat = 52.254


def get_random_points_in_warsaw(n):
    random_points = []
    for i in range(0, n):
        u = random.random()
        z = random.random()
        random_point = {
            "id": i,
            "lon": u * min_lon + (1 - u) * max_lon,
            "lat": z * min_lat + (1 - z) * max_lat,
        }
        random_points.append(random_point)
    return random_points

