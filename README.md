# CVRP Project
Practical implementation of Constrained Vehicle Routing Problem.

This is a student project for Systems Engineering course.

## Installation

Python 3.8 or newer is required. This project is OS-agnostic.

Install required pip packages:
```
pip install -r requirements.txt
```

## Solvers
Additionally, this project requires installation of any MLP solver supported by Pyomo. 

GLPK was used in testing, but Gurobi or CPLEX is recommended for performance reasons.

Full list of supported solvers is available from pyomo command (after installation):
```
pyomo help --solvers
```

To install GLPK on Ubuntu use:
```
sudo apt-get install python-glpk
sudo apt-get install glpk-utils
```

## Run the project
`python3 launch.pyw`